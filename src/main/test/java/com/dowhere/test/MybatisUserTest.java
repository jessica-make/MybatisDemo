package com.dowhere.test;

import com.dowhere.mapper.SqlMapper;
import com.dowhere.mapper.UserMapper;
import com.dowhere.pojo.SqlSessionAndIn;
import com.dowhere.pojo.User;
import com.dowhere.utils.SqlSessionUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author: Jessica
 * @create: 2022-04-19 9:14
 * @desc:
 **/

public class MybatisUserTest {

    @Test
    public void selectAll() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSession();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        //5.使用 SqlSession 创建 dao 接口的代理对象
        UserMapper mapper = session.getMapper(UserMapper.class);
        //6.使用代理对象执行查询所有方法
        List<User> users = mapper.findAll();
        for(User user : users) {
            System.out.println(user);
        }
        //7.释放资源
        session.close();
        in.close();
    }

    @Test
    public void insert() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSession();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);
        User user = new User();
        user.setUsername("老王");
        user.setPassword("123");
        user.setAge(26);
        user.setEmail("5209@qq.com");
        user.setSex("男");
        int result = mapper.insertUser(user);

        //Mybatis-config 使用的是原始JDBC，需要手动commit
        session.commit();

        System.out.println("result: "+result);

        //释放资源
        session.close();
        in.close();
    }

    @Test
    public void autoCommit() throws IOException {
        //1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        //3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        //4.使用 SqlSessionFactory 生产 SqlSession 对象
        //设置自动提交
        SqlSession session = factory.openSession(true);
        //5.使用 SqlSession 创建 dao 接口的代理对象
        UserMapper userDao = session.getMapper(UserMapper.class);
        //6.使用代理对象新增
        User user = new User();
        user.setUsername("老李");
        user.setPassword("123");
        user.setAge(25);
        user.setEmail("5209@qq.com");
        user.setSex("女");
        int res = userDao.insertUser(user);
        System.out.println("res: "+res);
        //7.释放资源
        session.close();
        in.close();
    }

    @Test
    public void selectById1() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSession();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        User user = new User();
        user.setUsername("admin");
        UserMapper mapper = session.getMapper(UserMapper.class);
        User userById = mapper.getUserById1(user);
        System.out.println("userById "+userById);

        session.close();
        in.close();
    }

    @Test
    public void selectById2() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        User user = new User();
        user.setUsername("'admin'");
        UserMapper mapper = session.getMapper(UserMapper.class);
        User userById = mapper.getUserById2(user);
        System.out.println("userById: "+userById);

        session.close();
        in.close();
    }

    @Test
    public void selectByMap() throws IOException{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);

        Map<String,Object> map = new HashMap<>();
        map.put("username","admin");
        map.put("password","123");

        User checkLoginByMap = mapper.checkLoginByMap(map);
        System.out.println("checkLoginByMap: "+ checkLoginByMap);

        session.close();
        in.close();
    }

    @Test
    public void selectByString() throws IOException{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);

        User checkLogin = mapper.checkLogin("admin","123");
        System.out.println("checkLogin: "+checkLogin);

        session.close();
        in.close();
    }

    @Test
    public void selectByParam() throws IOException{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);

        User checkLogin = mapper.checkLoginByParam("admin","123");
        System.out.println("checkLogin: "+checkLogin);

        session.close();
        in.close();
    }


    @Test
    public void selectByIdToMap() throws IOException{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);

        Map<String, Object> stringObjectMap = mapper.selectByIdToMap(1);
        System.out.println("stringObjectMap: "+ stringObjectMap);

        session.close();
        in.close();
    }


    @Test
    public void selectAllUserToMap() throws IOException{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);

        List<Map<String, Object>> maps = mapper.selectAllUserToMap();
        System.out.println("maps: "+ maps);

        session.close();
        in.close();
    }


    @Test
    public void getUserByUserName() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        SqlMapper mapper = session.getMapper(SqlMapper.class);
        List<User> admin = mapper.getUserByUserName("admin");
        System.out.println("admin:"+admin);

        session.close();
        in.close();
    }

    @Test
    public void insertBatchUser() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        List<User> list = new ArrayList<>();

        User user1 = new User(null,"小李子","123",15,"11@qq.com","男");
        User user2 = new User(null,"小梅子","456",16,"22@qq.com","女");
        User user3 = new User(null,"小华子","789",17,"33@qq.com","男");
        list.add(user1);
        list.add(user2);
        list.add(user3);

        UserMapper mapper = session.getMapper(UserMapper.class);
        int result = mapper.insertBatchUser(list);
        System.out.println("result:"+result);

        session.close();
        in.close();
    }

    @Test
    public void deleteBatchUser() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        UserMapper mapper = session.getMapper(UserMapper.class);

        mapper.deleteBatchUser(Arrays.asList(7,8));

        session.close();
        in.close();
    }

}
