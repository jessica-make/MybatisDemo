package com.dowhere.test;

import com.dowhere.mapper.TestGenCodeMapper;
import com.dowhere.pojo.TestGenCode;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author: Jessica
 * @create: 2022-04-21 19:23
 * @desc:
 **/

public class MybatisPageHelperTest {
    @Test
    public void testPageHelper() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TestGenCodeMapper mapper = sqlSession.getMapper(TestGenCodeMapper.class);
        //访问第一页，每页五条数据
        PageHelper.startPage(1,5);
        List<TestGenCode> testGenCodes = mapper.selectByExample(null);
        testGenCodes.forEach(System.out::println);
    }

    @Test
    public void testPageHelperByPageInfo() throws IOException {
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(is);
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        TestGenCodeMapper mapper = sqlSession.getMapper(TestGenCodeMapper.class);
        PageHelper.startPage(1, 5);
        List<TestGenCode> testGenCodes = mapper.selectByExample(null);
        PageInfo<TestGenCode> page = new PageInfo<>(testGenCodes,5);
        System.out.println(page);
    }





}
