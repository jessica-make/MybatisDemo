package com.dowhere.test;

import com.dowhere.mapper.CacheMapper;
import com.dowhere.pojo.Emp;
import com.dowhere.pojo.SqlSessionAndIn;
import com.dowhere.utils.SqlSessionUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author: Jessica
 * @create: 2022-04-21 13:40
 * @desc:
 **/

public class MybatisCacheEmpTest {

    /**
     * 测试一级缓存 使用同一个 SqlSession
     * @throws IOException
     */
    @Test
    public void selectAll() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        CacheMapper mapper = session.getMapper(CacheMapper.class);
        Emp empByEid1 = mapper.getEmpByEid(1);

        System.out.println("empByEid1: "+empByEid1);

        /**
         * 第二次不会输出sql 语句
         */
        Emp empByEid2 = mapper.getEmpByEid(1);

        System.out.println("empByEid2: "+empByEid2);

        session.close();
        in.close();
    }

    @Test
    public void selectAllByTwoSqlSession() throws IOException {
        SqlSessionAndIn sqlSession1 = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session1 = sqlSession1.getSqlSession();
        InputStream in1 = sqlSession1.getIn();

        SqlSessionAndIn sqlSession2 = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session2 = sqlSession2.getSqlSession();
        InputStream in2 = sqlSession1.getIn();

        CacheMapper mapper = session1.getMapper(CacheMapper.class);
        Emp empByEid1 = mapper.getEmpByEid(1);

        System.out.println("empByEid1: "+empByEid1);

        /**
         * 不同的SqlSession 不会缓存
         */
        CacheMapper mapper2 = session2.getMapper(CacheMapper.class);
        Emp empByEid2 = mapper2.getEmpByEid(1);

        System.out.println("empByEid2: "+empByEid2);

        session1.close();
        in1.close();

        session2.close();
        in2.close();
    }

    /**
     * 测试二级缓存，使用同一个 sessionFactory
     * @throws IOException
     */
    @Test
    public void testTwoLevelCache() throws IOException {
        //1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");

        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(in);
        SqlSession session1 = sessionFactory.openSession(true);
        CacheMapper mapper1 = session1.getMapper(CacheMapper.class);
        Emp empByEid1 = mapper1.getEmpByEid(1);

        System.out.println("empByEid1 "+empByEid1);
        session1.close();

        SqlSession session2 = sessionFactory.openSession(true);
        CacheMapper mapper2 = session2.getMapper(CacheMapper.class);
        Emp empByEid2 = mapper2.getEmpByEid(1);

        System.out.println("empByEid2 "+empByEid2);

        session2.close();
        in.close();
    }


}
