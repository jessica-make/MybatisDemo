package com.dowhere.test;

import com.dowhere.mapper.EmpMapper;
import com.dowhere.pojo.Emp;
import com.dowhere.pojo.SqlSessionAndIn;
import com.dowhere.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author: Jessica
 * @create: 2022-04-20 17:08
 * @desc:
 **/

public class MybatisEmpTest {

    @Test
    public void selectAll() throws IOException {
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        EmpMapper mapper = session.getMapper(EmpMapper.class);
        //1、全部查询
        List<Emp> empList = mapper.getEmpAll(null);

        //2、模糊查询
//        Emp emp = new Emp();
//        emp.setEmpName("张三");
//        emp.setAge(12);
//        List<Emp> empList = mapper.getEmpAll(emp);

        System.out.println("empList:"+empList);

        session.close();
        in.close();
    }

    @Test
    public void selectEmpAndDept() throws Exception{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        EmpMapper mapper = session.getMapper(EmpMapper.class);
        Emp empAndDept = mapper.getEmpAndDept(1);
        System.out.println("empAndDept:"+empAndDept);

        session.close();
        in.close();
    }


    @Test
    public void getEmpAndDeptThree() throws Exception{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        EmpMapper mapper = session.getMapper(EmpMapper.class);
        Emp empAndDept = mapper.getEmpAndDeptThree(1);
        //延迟加载
        //System.out.println("empAndDept:"+empAndDept.getEmpName());

        System.out.println("empAndDept:"+empAndDept.getDept());

        session.close();
        in.close();
    }
}
