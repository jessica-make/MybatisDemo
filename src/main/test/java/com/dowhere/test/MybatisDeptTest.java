package com.dowhere.test;

import com.dowhere.mapper.DeptMapper;
import com.dowhere.pojo.Dept;
import com.dowhere.pojo.SqlSessionAndIn;
import com.dowhere.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.io.InputStream;

/**
 * @author: Jessica
 * @create: 2022-04-21 8:10
 * @desc:
 **/

public class MybatisDeptTest {
    @Test
    public void selectDeptAndEmp() throws Exception{
        SqlSessionAndIn sqlSession = SqlSessionUtils.getSqlSessionAutoCommit();
        SqlSession session = sqlSession.getSqlSession();
        InputStream in = sqlSession.getIn();

        DeptMapper mapper = session.getMapper(DeptMapper.class);
        Dept deptAndEmp = mapper.getDeptAndEmp(1);
        System.out.println("deptAndEmp:"+deptAndEmp);

        session.close();
        in.close();
    }
}
