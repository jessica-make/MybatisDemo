package com.dowhere.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: Jessica
 * @create: 2022-04-20 16:56
 * @desc:
 **/
@Data
public class Emp implements Serializable {

    private Integer eid;

    private String empName;

    private Integer age;

    private String sex;

    private String email;

    private Dept dept;

}
