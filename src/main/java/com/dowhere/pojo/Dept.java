package com.dowhere.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author: Jessica
 * @create: 2022-04-20 16:57
 * @desc:
 **/
@Data
public class Dept {

    private Integer did;

    private String deptName;

    private List<Emp> emps;
}
