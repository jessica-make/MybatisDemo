package com.dowhere.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Jessica
 * @create: 2022-04-19 8:58
 * @desc: dev分支提交的代码
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Integer id;

    private String username;

    private String password;

    private Integer age;

    private String email;

    private String sex;


}
