package com.dowhere.pojo;

import lombok.Data;
import org.apache.ibatis.session.SqlSession;

import java.io.InputStream;

/**
 * @author: Jessica
 * @create: 2022-04-19 10:41
 * @desc:
 **/
@Data
public class SqlSessionAndIn {
   private SqlSession sqlSession;

   private InputStream in;
}
