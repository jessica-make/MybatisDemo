package com.dowhere.utils;

import com.dowhere.pojo.SqlSessionAndIn;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author: Jessica
 * @create: 2022-04-19 19:38
 * @desc:
 **/

public class SqlSessionUtils {
    /**
     * 手动提交
     * @return
     * @throws IOException
     */
    public static SqlSessionAndIn getSqlSession() throws IOException {
        SqlSessionAndIn sqlSessionAndIn = new SqlSessionAndIn();
        //1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        //3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        //4.使用 SqlSessionFactory 生产 SqlSession 对象
        sqlSessionAndIn.setSqlSession(factory.openSession());
        sqlSessionAndIn.setIn(in);
        return sqlSessionAndIn;
    }

    /**
     * 自动提交
     * @return
     * @throws IOException
     */
    public static SqlSessionAndIn getSqlSessionAutoCommit() throws IOException {
        SqlSessionAndIn sqlSessionAndIn = new SqlSessionAndIn();
        //1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        //2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        //3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        //4.使用 SqlSessionFactory 生产 SqlSession 对象
        sqlSessionAndIn.setSqlSession(factory.openSession(true));
        sqlSessionAndIn.setIn(in);
        return sqlSessionAndIn;
    }

}
