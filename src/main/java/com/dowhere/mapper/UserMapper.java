package com.dowhere.mapper;

import com.dowhere.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author: Jessica
 * @create: 2022-04-19 9:01
 * @desc:
 **/

@Mapper
public interface UserMapper {

    /**
     * 添加用户信息
     * @return
     */
    int insertUser(User user);

    /**
     * 批量添加用户信息
     * @return
     */
    int insertBatchUser(@Param("emps") List<User> users);

    /**
     * 批量删除用户信息
     */
    void deleteBatchUser(@Param("ids") List<Integer> ids);

    /**
     * 查询所有用户信息
     * @return
     */
    List<User> findAll();

    /**
     * 查询单个用户信息，#号查询,set赋值
     * @return
     */
    User getUserById1(User user);

    /**
     * 查询单个用户信息，$号查询，字符串拼接
     * @return
     */
    User getUserById2(User user);

    /**
     * 登录校验
     *
     * desc: Mybatis 在传入多个参数时，会将多个参数放到一个Map集合里面，
     *     key=arg0/param1,arg1/param2,value就是你传的值
     *
     * @param username
     * @param password
     * @return
     */
    User checkLogin(String username, String password);

    /**
     * 登录校验
     *
     * Map查询
     * @param map
     * @return
     */
    User checkLoginByMap(Map<String,Object> map);

    /**
     * 登录校验
     *
     * 注:如果是实体类的话，不需要带@Parms注解
     *
     * @Param 传入参数
     * @param username
     * @param password
     * @return
     */
    User checkLoginByParam(@Param("username") String username,@Param("password") String password);


    /**
     * 根据Id查询结果，以Map形式返回
     * @param id
     * @return
     */
    Map<String,Object> selectByIdToMap(@Param("id") Integer id);

    /**
     * 查询所有的用户，每一条用Map接收，放到一个List集合里面
     * @return
     */
    List<Map<String,Object>> selectAllUserToMap();

}
