package com.dowhere.mapper;

import com.dowhere.pojo.Emp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: Jessica
 * @create: 2022-04-20 16:58
 * @desc:
 **/
@Mapper
public interface EmpMapper {
    /**
     *查询员工表所有信息
     */
    List<Emp> getEmpAll(Emp emp);
    /**
     *     /**
     *      *查询员工表和对应的部门
     *      */

    Emp getEmpAndDept(@Param("eid") Integer eid);


    /**
     * 二次查询，获得员工表信息
     * @param eid
     * @return
     */
    Emp getEmpAndDeptThree(@Param("eid") Integer eid);
}
