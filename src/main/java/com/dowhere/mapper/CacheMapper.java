package com.dowhere.mapper;

import com.dowhere.pojo.Emp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: Jessica
 * @create: 2022-04-21 13:37
 * @desc:
 *
 **/
@Mapper
public interface CacheMapper {
    Emp getEmpByEid(@Param("eid") Integer id);
}
