package com.dowhere.mapper;

import com.dowhere.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: Jessica
 * @create: 2022-04-20 15:29
 * @desc:
 **/

@Mapper
public interface SqlMapper {
    /**
     * 根据用户名模糊查询
     */

    List<User> getUserByUserName(@Param("userName") String userName);

}
