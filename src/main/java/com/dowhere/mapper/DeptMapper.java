package com.dowhere.mapper;

import com.dowhere.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: Jessica
 * @create: 2022-04-20 16:58
 * @desc:
 **/
@Mapper
public interface DeptMapper {
    /**
     * 分步查询 员工以及员工所在的部门信息
     * @param did
     * @return
     */
    Dept selectDept(@Param("did") Integer did);

    /**
     * 获取部门以及部门所有的员工信息
     *
     * 处理一对多
     * @return
     */
    Dept getDeptAndEmp(@Param("did") Integer did);

}
